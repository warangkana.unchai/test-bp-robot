*** Settings ***
Resource  ../resources/BrowserSetup.robot
Resource  ../resources/TodoLocators.robot

*** Test Cases ***
Mark Todo As Done
    [Tags]    Regression Test
    Open Browser To Todo List
    Input Text  ${TODO_INPUT}  Test Todo
    Wait Until Element Is Visible  ${ADD_BUTTON}  10s
    Click Button  ${ADD_BUTTON}
    Capture Page Screenshot  # บันทึกภาพหน้าจอเมื่อกรณีผ่าน  
    Wait Until Element Is Visible  ${TO_DO_TASKS}  10s
    Click Element  ${TO_DO_TASKS}
    Capture Page Screenshot  # บันทึกภาพหน้าจอเมื่อกรณีผ่าน  
    Wait Until Page Contains  To-Do Tasks
    Page Should Contain  Test Todo
    Wait Until Element Is Visible  ${CHECK_BOX}  10s
    Click Element  ${CHECK_BOX}
    Capture Page Screenshot  # บันทึกภาพหน้าจอเมื่อกรณีผ่าน  
    Wait Until Element Is Visible  ${COMPLETE_TAB}  10s
    Click Element  ${COMPLETE_TAB}
    Wait Until Page Contains  To-Do Tasks
    Page Should Contain  Test Todo
    Capture Page Screenshot  # บันทึกภาพหน้าจอเมื่อกรณีผ่าน
    Wait Until Element Is Visible  ${DELETE_BUTTON}  10s
    Click Element  ${DELETE_BUTTON}
    Wait Until Page Does Not Contain  Test Todo  timeout=10s
    Page Should Not Contain  Test Todo
    Capture Page Screenshot  # บันทึกภาพหน้าจอเมื่อกรณีผ่าน
    [Teardown]  Close Browsers
