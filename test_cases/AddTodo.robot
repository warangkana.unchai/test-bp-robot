*** Settings ***
Resource  ../resources/BrowserSetup.robot
Resource  ../resources/TodoLocators.robot

*** Test Cases ***
Add New Todo
    [Tags]    Regression Test
    Open Browser To Todo List
    Input Text  ${TODO_INPUT}  Test Todo
    Wait Until Element Is Visible  ${ADD_BUTTON}  10s
    Click Button  ${ADD_BUTTON}
    Capture Page Screenshot  # บันทึกภาพหน้าจอเมื่อกรณีผ่าน
Tab Todo Tasks 
    [Tags]    Regression Test
    Wait Until Element Is Visible  ${TO_DO_TASKS_TAB}  10s
    Click Element  ${TO_DO_TASKS_TAB} 
    Wait Until Page Contains  To-Do Tasks
    Page Should Contain  Test Todo
    Capture Page Screenshot  # บันทึกภาพหน้าจอเมื่อกรณีผ่าน
    [Teardown]  Close Browser