*** Settings ***
Resource  ../resources/BrowserSetup.robot
Resource  ../resources/TodoLocators.robot

*** Test Cases ***
Check tabs to Page
    [Tags]    Regression Test
    Open Browser To Todo List
    Wait Until Element Is Visible  ${COMPLETE_TAB}  10s
    Click Element  ${COMPLETE_TAB}
    Capture Page Screenshot  # บันทึกภาพหน้าจอเมื่อกรณีผ่าน
    Wait Until Element Is Visible  ${TO_DO_TASKS}  10s
    Click Element  ${TO_DO_TASKS}
    Capture Page Screenshot  # บันทึกภาพหน้าจอเมื่อกรณีผ่าน
    Wait Until Element Is Visible  ${ADD_ITEM_TAB}  10s
    Click Element  ${ADD_ITEM_TAB}
    Capture Page Screenshot  # บันทึกภาพหน้าจอเมื่อกรณีผ่าน
    [Teardown]  Close Browser