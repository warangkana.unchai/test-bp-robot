<!-- --- robot run pass ---  -->
robot -d results test_cases/

<!-- --- docker img ---  -->
docker build -t robotframework07 .
<!-- docker run -it --rm -v /Users/warangkanaunchai/TodoListTests:/TodoListTests robotframework07 bash -c "robot --outputdir /TodoListTests/results /TodoListTests/test_cases" -->
docker run -it --rm -v  /Users/warangkanaunchai/test-bp-robot:/test-bp-robot robotframework07 bash -c "robot --outputdir /test-bp-robot/results /test-bp-robot/test_cases"