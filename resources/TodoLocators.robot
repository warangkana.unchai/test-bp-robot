*** Variables ***
${TODO_INPUT}  css=input[type="text"]
${TODO_ITEM}   css=ul li
${TODO_CHECKBOX}  css=ul li input
${TO_DO_TASKS}  css=.mdl-tabs__tab:nth-child(2) > .mdl-tabs__ripple-container
${ADD_BUTTON}  css=button.mdl-button.mdl-button--fab
${DELETE_BUTTON}  xpath=//button[contains(text(), 'Delete')]
${DELETE_BUTTON_TESTS}  xpath=//span[@id='text-1']
${ADD_ITEM_TAB}  css=.mdl-tabs__tab:nth-child(1) > .mdl-tabs__ripple-container
${TO_DO_TASKS_TAB}  css=.mdl-tabs__tab:nth-child(2) > .mdl-tabs__ripple-container
${COMPLETE_TAB}  css=.mdl-tabs__tab:nth-child(3) > .mdl-tabs__ripple-container
${CHECK_BOX}  css=.mdl-checkbox__ripple-container
${CSV File}    data/test_data.csv