FROM python:3

# ติดตั้ง Robot Framework, SeleniumLibrary และ dependencies ที่จำเป็น
RUN pip3 install robotframework robotframework-seleniumlibrary

# เพิ่ม key ของ Google Chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -

# เพิ่ม repository ของ Google Chrome
RUN echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list

# ติดตั้ง Chrome browser   
RUN apt-get update && apt-get install -y wget unzip \
    && wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb \
    && dpkg -i google-chrome-stable_current_amd64.deb; apt-get -fy install

# ติดตั้ง ChromeDriver
RUN wget https://edgedl.me.gvt1.com/edgedl/chrome/chrome-for-testing/117.0.5938.88/linux64/chrome-linux64.zip && \
    unzip chrome-linux64.zip && \
    cp chrome-linux64/chrome /usr/bin/chromedriver && \
    chmod +x /usr/bin/chromedriver && \
    apt-get clean && \
    rm chrome-linux64.zip

# Clean up
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* google-chrome-stable_current_amd64.deb chromedriver_linux64.zip
